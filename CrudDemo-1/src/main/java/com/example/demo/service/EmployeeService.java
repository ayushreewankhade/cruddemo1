package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.customException.EmptyInputException;
import com.example.demo.dto.EmployeeDto;
import com.example.demo.entity.Employee;
import com.example.demo.repos.EmployeeCrudRepo;

@Service
public class EmployeeService implements EmployeeServiceInterface{

	@Autowired
	private EmployeeCrudRepo crudRepo;

	@Override
	public Employee addEmployee(Employee employee) {
		if(employee.getName().isEmpty() || employee.getName().length()==0) {
			throw new EmptyInputException("601", "please send proper name");
		}
		Employee savedEmployee= crudRepo.save(employee);
		return savedEmployee;
	
	}

	@Override
	public List<Employee> getAllEmployees() {
		return crudRepo.findAll();
	}

	@Override
	public Optional<EmployeeDto> getEmpById(Long empId) {
		return crudRepo.findById(empId, EmployeeDto.class);
	}

	@Override
	public void deleteEmpById(Long empId) {
	    crudRepo.deleteById(empId);
			
		}
		
	}

	

	

	

	
	

