package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.entity.Employee;

public interface EmployeeServiceInterface {

	public Employee addEmployee(Employee employee);

	public List<Employee> getAllEmployees();
	
	public Optional<EmployeeDto> getEmpById(Long empId);

	public void deleteEmpById(Long empId);

	



	
	
}
