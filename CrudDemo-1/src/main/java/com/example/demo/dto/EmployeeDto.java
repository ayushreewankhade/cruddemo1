package com.example.demo.dto;

public class EmployeeDto {
	
	public EmployeeDto() {
		
	}
	private Long empId;
	private String name;
	private String address;
	
	public Long getId() {
		return empId;
	}
	public void setId(Long empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
