package com.example.demo.dto;

public class SuccessResponseDto {

	private String message;
	private Object data;
	private String msgKey;

	public SuccessResponseDto() {
		
	}
	
	public SuccessResponseDto(String message, Object data) {

		super();
		this.message = message;
		this.data = data;

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMsgKey() {
		return msgKey;
	}

	public void setMsgKey(String msgKey) {
		this.msgKey = msgKey;
	}

}
