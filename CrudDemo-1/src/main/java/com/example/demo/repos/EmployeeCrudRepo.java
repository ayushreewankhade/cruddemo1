package com.example.demo.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.entity.Employee;


public interface EmployeeCrudRepo extends JpaRepository<Employee, Long> {
	
	Optional<EmployeeDto> findById(Long id, Class<EmployeeDto> IMasterDetailDto);
	
}
