package com.example.demo.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.ErrorResponseDto;
import com.example.demo.dto.SuccessResponseDto;
import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeServiceInterface;

@RestController
@RequestMapping("/code")
public class EmployeeController {

	@Autowired
	private EmployeeServiceInterface employeeServiceInterface;
	
	@PostMapping("/save")
	public ResponseEntity<Employee> addEmployee(@RequestBody Employee employee){
		Employee employeeSaved= employeeServiceInterface.addEmployee(employee);
		return new ResponseEntity<Employee>(employeeSaved, HttpStatus.CREATED);
	}
	
	@GetMapping("/all")
	public ResponseEntity<List<Employee>> getAllEmployees(){
		List<Employee> listOfAllEmps= employeeServiceInterface.getAllEmployees();
		return new ResponseEntity<List<Employee>>(listOfAllEmps, HttpStatus.OK);
	}

	@GetMapping("emp/{empid}")
	public ResponseEntity<?> getUserById(@PathVariable(value = "empid") Long empId) throws NoSuchElementException {
		try {

			Optional<EmployeeDto> retrivedEmp = employeeServiceInterface.getEmpById(empId);
			return new ResponseEntity<>(new SuccessResponseDto("Success", retrivedEmp), HttpStatus.OK);

		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "userNotFound"), HttpStatus.NOT_FOUND);
		}
	}
	
	/*public ResponseEntity<Employee> getEmpBId(@PathVariable("empid")Long empId ){
		Employee empRetrived= employeeServiceInterface.getEmpById(empId);
		return new ResponseEntity<Employee>(empRetrived, HttpStatus.OK);
	}*/

	@DeleteMapping("/delete/{empid}")
	public ResponseEntity<Void> deleteEmpBId(@PathVariable("empid")Long empId ){
		employeeServiceInterface.deleteEmpById(empId);
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}
	
	@PutMapping("/update/{empid}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable("empid")Long empId, @RequestBody Employee employee) throws EmptyResultDataAccessException{
		Employee employeeSaved= employeeServiceInterface.addEmployee(employee);
		return new ResponseEntity<Employee>(employeeSaved, HttpStatus.CREATED);
	}
	
}
